# mybatisplus-dynamic-datasource-spring-boot-starter

#### 项目介绍
扩展 dynamic-datasource-spring-boot-starter 集成 MybatisPlus 3.0-RC1实现动态数据源
支持直接使用ServiceImpl的公有方法时动态切换数据源

按照使用说明完成操作，可实现DS注解在任意位置：
1. 注解在业务服务类上
2. 注解在业务服务方法上
3. 注解在业务服务类上，但直接使用的是ServiceImpl中公有方法，无需进行包装
4. 注解在自己的Mapper上，但使用BaseMapper的公有方法
5. 注解在自己的Mapper上，但使用自定的方法
6. 注解在自己Mapper的自定方法上

这些场景适合与：同一个业务方法中，需要多个不同数据源的Mapper协作时。  
详见测试用例：[MybatisplusDynamicDatasourceSpringBootStarterApplicationTests.java](https://gitee.com/PrincessOfHuang/mybatisplus-dynamic-datasource-spring-boot-starter/blob/master/src/test/java/com/skytech/ssr/dynamicdb/MybatisplusDynamicDatasourceSpringBootStarterApplicationTests.java)

#### 使用说明

1. 扩展了 dynamic-datasource-spring-boot-starter 请参照：[mybatisplus-dynamic-datasource-spring-boot-starter](https://gitee.com/PrincessOfHuang/mybatisplus-dynamic-datasource-spring-boot-starter)
2. 扩展了 Mybatis-Plus的PageMapperProxy.java,开放mapperInterface的只读接口，请参照：[PageMapperProxy.java](https://gitee.com/PrincessOfHuang/mybatisplus-dynamic-datasource-spring-boot-starter/blob/master/src/main/java/com/baomidou/mybatisplus/core/override/PageMapperProxy.java)
3. 实现 dynamic-datasource-spring-boot-starter 中的自定义注解解析器（DynamicAnnotationResolver）请参照：[DynamicAnnotationResolver.java](https://gitee.com/PrincessOfHuang/mybatisplus-dynamic-datasource-spring-boot-starter/blob/master/src/main/java/com/skytech/ssr/dynamicdb/config/MyAnnotationResolver.java)
4. 覆写 DynamicDataSourceAnnotationAdvisor 注册 MyAnnotationResolver
    ```
    @Bean
    public DynamicDataSourceAnnotationAdvisor dynamicDatasourceAnnotationAdvisor() {
        DynamicDataSourceAnnotationInterceptor dynamicInterceptor = new DynamicDataSourceAnnotationInterceptor();
        dynamicInterceptor.setCustomAnnotationResolver(new MyAnnotationResolver());
        return new DynamicDataSourceAnnotationAdvisor(dynamicInterceptor);
    }
    ```

#### 参与贡献

1. Fork [dynamic-datasource-spring-boot-starter](https://gitee.com/baomidou/dynamic-datasource-spring-boot-starter)



#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)