package com.skytech.ssr.dynamicdb.secondary.domain.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * 部门表
 */
@TableName("SYS_DEPT")
public class Dept implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long pkid;
    @TableField("DEPT_NAME")
    private String name;
    @TableLogic
    private Integer disable;

    public Long getPkid() {
        return pkid;
    }

    public void setPkid(Long pkid) {
        this.pkid = pkid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }
}
