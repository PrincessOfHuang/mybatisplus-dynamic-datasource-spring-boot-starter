package com.skytech.ssr.dynamicdb.secondary.domain.mappers;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.skytech.ssr.dynamicdb.secondary.domain.model.Dept;


@DS("secondary")
public interface DeptMapper extends BaseMapper<Dept> {

    void saveDept(Dept dept);
}
