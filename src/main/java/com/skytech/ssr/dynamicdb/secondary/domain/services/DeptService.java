package com.skytech.ssr.dynamicdb.secondary.domain.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.skytech.ssr.dynamicdb.secondary.domain.mappers.DeptMapper;
import com.skytech.ssr.dynamicdb.secondary.domain.model.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
//@DS("secondary")
public class DeptService extends ServiceImpl<DeptMapper, Dept> {
    @Autowired
    private DeptMapper deptMapper;

    public IPage<Dept> findUserByMapper(Page<Dept> page, JSONObject params) {
/*        PageHelper.startPage((int) page.getCurrent(), (int) page.getSize());
        return PageWrapper.wrapper(page, new PageInfo(deptMapper.findDepts(params)));*/
        return null;
    }

//    @DS("secondary")
    public void saveDept(Dept dept) {
        deptMapper.insert(dept);
    }

    public void mapperSaveDept(Dept dept) {
        deptMapper.saveDept(dept);
    }
}
