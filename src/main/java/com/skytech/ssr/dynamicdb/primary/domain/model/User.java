package com.skytech.ssr.dynamicdb.primary.domain.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * 用户表
 */
@TableName("SYS_USER")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long pkid;
    @TableField("LOGIN_NAME")
    private String name;
    private String token;
    private Integer age;
    @TableField("CHINA_NAME")
    private String chinaName;
    //枚举类型默认使用name作为值存入数据库 覆写ToString方法实现使用值存数据库
    @JSONField(serialzeFeatures = SerializerFeature.WriteEnumUsingToString)
    private PositionEm position;
    @TableLogic
    private Integer disable;

    public Long getPkid() {
        return pkid;
    }

    public void setPkid(Long pkid) {
        this.pkid = pkid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getChinaName() {
        return chinaName;
    }

    public void setChinaName(String chinaName) {
        this.chinaName = chinaName;
    }

    public void setPosition(PositionEm position) {
        this.position = position;
    }

    public PositionEm getPosition() {
        return position;
    }

    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }
}
