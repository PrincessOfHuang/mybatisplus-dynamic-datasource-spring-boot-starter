package com.skytech.ssr.dynamicdb.primary.domain.mappers;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.skytech.ssr.dynamicdb.primary.domain.model.User;

import java.util.List;

public interface UserMapper extends BaseMapper<User> {

}
