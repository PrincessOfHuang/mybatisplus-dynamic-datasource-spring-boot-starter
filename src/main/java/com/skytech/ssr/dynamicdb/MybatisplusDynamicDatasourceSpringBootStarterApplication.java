package com.skytech.ssr.dynamicdb;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
@ComponentScan(basePackages = {"com.baomidou","com.skytech"})
public class MybatisplusDynamicDatasourceSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisplusDynamicDatasourceSpringBootStarterApplication.class, args);
    }
}
