package com.skytech.ssr.dynamicdb;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.skytech.ssr.dynamicdb.primary.domain.model.User;
import com.skytech.ssr.dynamicdb.primary.domain.services.UserService;
import com.skytech.ssr.dynamicdb.secondary.domain.model.Dept;
import com.skytech.ssr.dynamicdb.secondary.domain.services.DeptService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MybatisplusDynamicDatasourceSpringBootStarterApplicationTests {

    @Test
    public void contextLoads() {
    }


    @Autowired
    private UserService userService;

    @Autowired
    private DeptService deptService;


    @Test
    public void 主库插入记录(){
        User user = new User();
        user.setName("JACK4");
        user.setAge(33);
        user.setToken("123456789");
        user.setDisable(0);

        userService.save(user);

        System.out.println(JSONObject.toJSONString(user));
        List<User> userList = userService.list(null);
        System.out.println(JSONObject.toJSONString(userList));
    }


    @Test
    public void 从库插入记录(){
        Dept dept = new Dept();
        dept.setName("研究院44");
        dept.setDisable(0);

        deptService.save(dept);

        System.out.println(JSONObject.toJSONString(dept));


        IPage<Dept> deptList = deptService.page(new Page<>(1,3),null);
        System.out.println(JSONObject.toJSONString(deptList));
    }


    @Test
    public void 同时操作主从库(){
        主库插入记录();
        从库插入记录();
    }

    @Test
    public void 注解在服务类上(){
        Dept dept = new Dept();
        dept.setName("注解在服务类上");
        dept.setDisable(0);

        deptService.saveDept(dept);
    }

    @Test
    public void 注解在服务方法上(){
        Dept dept = new Dept();
        dept.setName("注解在服务方法上");
        dept.setDisable(0);

        deptService.saveDept(dept);
    }

    @Test
    public void 注解在服务类上但使用父类的方法(){
        Dept dept = new Dept();
        dept.setName("注解在服务类上但使用父类的方法");
        dept.setDisable(0);

        deptService.save(dept);
    }


    @Test
    public void 注解在Mapper上但使用BaseMapper的方法(){
        Dept dept = new Dept();
        dept.setName("注解在Mapper上但使用BaseMapper的方法");
        dept.setDisable(0);

        deptService.save(dept);
    }

    @Test
    public void 注解在Mapper上但使用自定方法(){
        Dept dept = new Dept();
        dept.setPkid(new Date().getTime());
        dept.setName("注解在Mapper上但使用自定方法");
        dept.setDisable(0);

        deptService.mapperSaveDept(dept);
    }

    @Test
    public void 注解在Mapper的自定方法(){
        Dept dept = new Dept();
        dept.setPkid(new Date().getTime());
        dept.setName("注解在Mapper上使用自定方法");
        dept.setDisable(0);

        deptService.mapperSaveDept(dept);
    }


}
